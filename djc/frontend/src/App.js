import React from 'react'
import Login from './Login'
import Route from './EnsureLogin'
import Main from './Main'


export default () => 
    <div>
      <Route component={ Login } path="/login" />
      <Route component={ Main } path="/" />
    </div>

import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import serializeForm from 'form-serialize'
import PropTypes from 'prop-types'
import DetailInput from './DetailInput'
import MarkerMap from './AnotherMap'
import { asYouType } from 'libphonenumber-js'


class EditContact extends Component {

   static isPrivate = true
    
   static propTypes = {
    contact: PropTypes.object,
    removeContact: PropTypes.func, 
  }
  
  constructor(props) {
    super(props)
    this.state = {
          contact: this.props.contact ? this.props.contact : { id:'', firstName: '', lastName:'', address: '', organization:'',
                                                                location: {type: 'Point', coordinates: [62.601, 29.763] },
                                                                details: [{detailName: 'Phone', detailValue: ''}] }
        }
  }
   
  handleSubmit = (e) => {
    e.preventDefault()
    const values = serializeForm(e.target, { hash: true })
    if (this.props.onEditContact){
        if (values.details) {
          values.details = values.details.map((detail, idx) => {
              return { ...this.state.contact.details[idx], detailValue: detail }
          })}
        if (this.state.contact.location) 
           values.location = this.state.contact.location 
      this.props.onEditContact(values) }
  }
  
 onDeleteContact = (e) => {
   if (this.props.removeContact) { 
        this.props.removeContact(this.state.contact)
   }
 }    

 addDetail = (e) => {
    e.preventDefault()
    const newDetails = this.state.contact.details.concat([{ detailName: 'Phone', detailValue: '' }])
    this.setState((state) => ({
      contact: { ...state.contact, details: newDetails }
    }))
  }
  
  onDetailChange = (e, chgIdx) => {
      const number = new asYouType().input(e.target.value)
      const newDetail = {...this.state.contact.details[chgIdx], detailValue: number }
      const newDetails = this.state.contact.details.filter((d, idx) => chgIdx !== idx).concat([ newDetail ])
      this.setState((state) => ({
         contact:  {...this.state.contact, details: newDetails }
    })) 
  }
  
  onDeleteDetail = (detail, remIdx, e) => {
    e.preventDefault()
    const newDetails = this.state.contact.details.filter((d, idx) => remIdx !== idx)
    this.setState((state) => ({
      contact:  {...this.state.contact, details: newDetails }
    }))
  }

  retPosition = (newCoords) => {
      const newLocation = { coordinates: [newCoords.lat, newCoords.lng], type: 'Point'}
      this.setState((state) => ({
            contact:  {...this.state.contact, location: newLocation }
    }))
   }

  render() {
    return (
      <div>
        <Link className='close-create-contact' to='/'>Cancel</Link>
        <form onSubmit={this.handleSubmit} className='create-contact-form'>
          <div className='create-contact-details'>
          <input type='hidden' name='id' value={ this.state.contact.id }/>
            <label>
            Name: 
            <input type='text' name='firstName' defaultValue={  this.state.contact.firstName } placeholder='Name'/> </label>
             <label>
            Last name: 
            <input type='text' name='lastName' defaultValue={  this.state.contact.lastName } placeholder='Last Name'/> </label>
            
           <DetailInput details={this.state.contact.details} 
                    handleAddDetail={this.addDetail} 
                    handleRemoveDetail={this.onDeleteDetail}
                    handleChange={this.onDetailChange} />
          <label>

            Organization: <input type='text' name='organization' defaultValue={  this.state.contact.organization } placeholder='Organization'/></label>
           <label>
            Address: 
            <input type='text' name='address' defaultValue={  this.state.contact.address } placeholder='Address'/></label>
            
            <label>Location: </label>
            
             <MarkerMap point={this.state.contact.location}
                        retPosition={this.retPosition}/>
         
            
            <div>
            <button>Save Contact</button> 
                        {this.props.removeContact && (
              <button onClick={ (e) => { if(window.confirm('Delete contact?')) {
                                            this.onDeleteContact() }
                                        else { e.preventDefault() }
                                }}>Delete Contact</button>
              )} </div>
          </div>
        </form>
      </div>
    )
  }
}

export default EditContact
import React, { Component } from 'react'
import { login, register }  from './utils/api'

const PRIVATE_ROOT = '/'
const PUBLIC_ROOT = '/login'

class Login extends Component {
     
    static isPrivate = false
    
    constructor(props) {
        super(props)
        this.state = { 
            errors: [],
            username: '',
            password: ''
        }
        
    } 
    
    onUsername = (e) => {
        const value = e.target.value
        this.setState((state) => ({
                ...state, username: value
        }))
    }
     
    onPassword = (e) => {
        const value = e.target.value
        this.setState((state) => ({
                ...state, password: value
        }))
    }
    
    registerUser = (e) => {
        e.preventDefault()
        const values = {username: this.state.username, password: this.state.password}
        register(values).then((resp) => {
            if (resp.non_field_errors) {
                this.setState((state) => ({errors: resp.non_field_errors }))
            }
            else {
                const { username } = resp
                if ( typeof username === 'object' )
                     this.setState((state) => ({errors:username }))
                else {
                    return (this.handleLogin())
                }     
            }
        })
    }
    
    handleLogin = () => {
        const values = {username: this.state.username, password: this.state.password}
        login(values).then((data) => {
            if (data.non_field_errors) {
                this.setState((state) => ({errors: data.non_field_errors }))
            }
            return data.token ?
                this.props.history.push(PRIVATE_ROOT)
                :  this.props.history.push(PUBLIC_ROOT)
        })
    }
    handleSubmit = (e) => {
        e.preventDefault()
        const values = {username: this.state.username, password: this.state.password}
        login(values).then((data) => {
            if (data.non_field_errors) {
                this.setState((state) => ({errors: data.non_field_errors }))
            }
            return data.token ?
                this.props.history.push(PRIVATE_ROOT)
                :  this.props.history.push(PUBLIC_ROOT)
        })
    }
    
    render() {
        return (<div>
             <form className='create-contact-form'>
                  <div className='create-contact-details'>
               <label> Username:
               <input type='text' name='username' onChange={this.onUsername} 
                    value={this.state.username} placeholder="Enter your Username"
                 /> </label>
               <label> Password:
                 <input type='password' name='password' onChange={this.onPassword}
                 value={this.state.password} placeholder="Enter your Password"
                   /> </label>
                  <div>{ this.state.errors.map((error, idx) => (
                        <label key={idx}> { error } </label>
                    ))} </div> 
                <div> <button onClick={this.handleSubmit}>Login</button> <button onClick={this.registerUser}>Register</button>
                    </div>
                </div>
               </form>
          </div>
        );
    }
}

export default Login;
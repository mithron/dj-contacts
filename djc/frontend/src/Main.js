import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import ListContacts from './ListContacts'
import EditContact from './EditContact'
import Login from './Login'

import * as ContactsAPI from './utils/api'

class Main extends Component {
  
  static isPrivate = true
  
  constructor() {
    super();
    this.state = {
      contacts: [],
      token: localStorage.getItem('token') || ''  }
  }

  componentDidMount() {
    ContactsAPI.getAll(this.state.token).then((contacts) => {
      this.setState({ contacts })
    })
  }

  removeContact = (contact) => {
    this.setState((state) => ({
      contacts: state.contacts.filter((c) => c.id !== contact.id)
    }))
    ContactsAPI.remove(contact, this.state.token)
  }
  
  editContact = (contact) => {
   let later = []
   if (contact.details) {
     const oldContact = this.state.contacts.find((c) => c.id.toString() === contact.id )
     oldContact.details.forEach((olddetail) => {
       if (! (contact.details.find((d) => d.id === olddetail.id ))) {
        later.push(ContactsAPI.detailRemove(olddetail, this.state.token)) 
       }
     })}
   Promise.all(later).then( () => {
    ContactsAPI.update(contact, this.state.token).then(
     (contact) => {
          this.setState((state) => ({
            contacts: state.contacts.filter((c) => c.id !== contact.id).concat([ contact ])
          }))
      })})
  }
  
  createContact(contact) {
    ContactsAPI.create(contact, this.state.token).then(contact => {
      this.setState((state) => ({
        contacts: state.contacts.concat([ contact ]),
      }))
    })
  }
  
  render() {
    return (
      <div>
       <Route path='/login' render={(history) => (
          <Login
            onLogin={(credentials) => {
              this.login(credentials)
              history.push('/')  
            }}/>
        )}/>
       
          <Route exact path='/' render={() => (
            <ListContacts
              onDeleteContact={this.removeContact}
              contacts={this.state.contacts}
              onEditContact={this.editContact}
            />
          )}/>
          
          <Route path='/create' render={({ history }) => (
            <EditContact
              onEditContact={(contact) => {
                this.createContact(contact)
                history.push('/')
              }}/>
          )}/>
              
          <Route path='/contact/:id' render={({ history, match }) => (
            <EditContact
            contact={this.state.contacts.find((c) => c.id.toString() === match.params.id.slice(1))}
            removeContact={this.removeContact}
            onEditContact={(contact) => {
                this.editContact(contact)
                history.push('/')
              }}
            />
          )}/>
  
        </div>
    )
  }
}

export default Main
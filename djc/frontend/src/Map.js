import React, { Component } from 'react'
import { Map, TileLayer, Marker } from 'react-leaflet';
import PropTypes from 'prop-types'


class markerMap extends Component {
    
   static isPrivate = true
    
   static propTypes = {
    point: PropTypes.object,
    retPosition: PropTypes.func.isRequired
  }
  
  constructor(props) {
      super(props)
      this.retPosition = this.props.retPosition
      this.state = {
          markerPosition: this.props.point ? this.props.point.coordinates : [62.601, 29.763] // (Joensuu)
        }
  }
  
  updatePosition = () => {
    const { lat, lng } = this.refs.marker.leafletElement.getLatLng()
    this.setState({
       markerPosition: [lat, lng],
    })
    this.retPosition([lat, lng ])
  }

  render() {
    const position = this.state.markerPosition
    const markerPosition = this.state.markerPosition

    return (
        <div>
      <Map center={position} zoom={10}>
        <TileLayer
          attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker
          draggable={true}
          onDragend={this.updatePosition}
          position={markerPosition}
          ref="marker"/>
      </Map>
      </div>
    )
  }
}

export default markerMap
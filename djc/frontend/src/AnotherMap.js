import React, { Component } from 'react'
import L from 'leaflet'
import 'leaflet/dist/leaflet.css'
import PropTypes from 'prop-types'

// BUG https://github.com/Leaflet/Leaflet/issues/4968
import iconRetinaUrl from 'leaflet/dist/images/marker-icon-2x.png'
import iconUrl from 'leaflet/dist/images/marker-icon.png'
import shadowUrl from 'leaflet/dist/images/marker-shadow.png'

const MAP_ID = 'map'

L.Marker.prototype.options.icon = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
})

class Map extends Component {
  
   static isPrivate = true
    
   static propTypes = {
    point: PropTypes.object,
    retPosition: PropTypes.func.isRequired
  }

    constructor(props) {
        super(props);
        this.map = null
        this.retPosition = this.props.retPosition
        this.state = {
          markerPosition: this.props.point ? 
            this.props.point.coordinates 
            // [62.601, 29.763] 
            : [62.601, 29.763] // (Joensuu)
        }
    }

    componentDidMount() {
        this.initLeaflet()
    }


    initLeaflet() {
        const map = L.map(MAP_ID)
        const osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        const osm = new L.TileLayer(
            osmUrl, {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            }
        )
        map.setView(new L.LatLng(this.state.markerPosition[0], this.state.markerPosition[1]), 9)
        map.addLayer(osm)
        map.scrollWheelZoom.disable()
        let retPosition = this.retPosition
        var marker = new L.marker(this.state.markerPosition, { draggable: true }).addTo(map)
        marker.on('moveend', function(e) {
            retPosition(e.target._latlng)
        })
        this.map = map;
    }

    render() {
        return <div id={MAP_ID} />
    }

}


export default Map

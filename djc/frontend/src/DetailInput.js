import React, { Component } from 'react'
import PropTypes from 'prop-types'

class DetailInput extends Component {
   static propTypes = {
    details: PropTypes.array.isRequired,
    handleRemoveDetail: PropTypes.func.isRequired,
    handleAddDetail: PropTypes.func.isRequired,
    handleChange: PropTypes.func.isRequired,
  }
  
  render() {
    const {handleRemoveDetail, handleAddDetail, details, handleChange} = this.props
    return ( <div>
         { details.map((detail, idx) => (
             <div key={idx}>
                <label>
            <button onClick={(e) => { handleRemoveDetail(detail, idx, e) }}> - </button> 
            {detail.detailName}: 
            <input className='contact-detail-inline' type='text' 
                onChange={(e) => { handleChange(e, idx)}} name={ 'details[' + idx.toString() +']' } 
                value={detail.detailValue } placeholder={detail.detailName} />
            </label> </div>
          ))}
          <div>
          <button onClick={handleAddDetail}> Add Phone </button></div>
    </div> )
  }
}

export default DetailInput
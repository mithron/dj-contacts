import React, { Component } from 'react'
import Login from './Login'
import Route from './EnsureLogin'
import Main from './Main'

import * as ContactsAPI from './utils/api'

class MainRouter extends Component {
    
    static isPrivate = false
    
    isAuthenticated = () => this.state.token 
    
    constructor() {
        super();
        this.state = {
          token: ''
        }
    }
    
    login = (credentials) => {
      ContactsAPI.login(credentials)
      .then((token) => {
        this.setState({ token: token })
      })
    }
    
    render() {
      return ( <div>
                  <Route path='/login' render={(history) => (
                        <Login
                         isAuthenticated={this.isAuthenticated}
                          onLogin={(credentials) => {
                            this.login(credentials)
                            history.push('/')  
                          }}/>
                   )}/>
                  <Route path="/" render={() => (
                        <Main token= { this.state.token }
                        isAuthenticated = {this.isAuthenticated}
                        />
                    )}/>
                </div>
              )
      
    }
}

export default MainRouter
const api = process.env.REACT_APP_CONTACTS_API_URL || '/api/v0'

const headers = {
  'Accept': 'application/json',
}

export const login = (credentials) => 
  fetch(`${api}/auth`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  })
  .then(res => res.json())
  .then((data) => {
      if (data.token) {
         localStorage.setItem('token', 'Token ' + data.token) }
      return data
  })

export const register = (credentials) => 
  fetch(`${api}/register`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  }).then(res => res.json())

export const getAll = (token) =>
  fetch(`${api}/contacts/`, { headers: {
    'Authorization': token,
    ...headers
  } })
    .then(res => res.json())

export const remove = (contact, token) =>
  fetch(`${api}/contacts/${contact.id}/`, { method: 'DELETE', headers: {
     'Authorization':token,
    ...headers
  } })
    
export const update = (body, token) =>
  fetch(`${api}/contacts/${body.id}/`, {
    method: 'PUT',
    headers: {
       'Authorization': token,
       ...headers,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  }).then(res => res.json())
  
export const create = (body, token) =>
  fetch(`${api}/contacts/`, {
    method: 'POST',
    headers: {
       'Authorization': token,
       ...headers,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  }).then(res => res.json())
  
export const detailRemove = (detail, token) =>
  fetch(`${api}/details/${detail.id}/`, { method: 'DELETE', headers: {
     'Authorization': token,
    ...headers
  } })
import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'
import escapeRegExp from 'escape-string-regexp'
import sortBy from 'sort-by'

class ListContacts extends Component {
  
  static isPrivate = true
  
  static propTypes = {
    contacts: PropTypes.array.isRequired,
    onDeleteContact: PropTypes.func.isRequired, 
    onEditContact: PropTypes.func.isRequired,
  }
  
  
  logout = () => {
    localStorage.removeItem('token')
    return <Redirect to='/login' />
  }
  
  state = {
    query: ''
  }

  updateQuery = (query) => {
    this.setState({ query: query.trim() })
  }

  clearQuery = () => {
    this.setState({ query: '' })
  }
  
  showDetail = (contact) => {
     window.location.hash = '#/contact/:'+contact.id
  }

  render() {
    const { contacts, onDeleteContact } = this.props
    const { query } = this.state
    
    let showingContacts = []
    if (query) {
      const match = new RegExp(escapeRegExp(query), 'i')
      showingContacts = contacts.filter((contact) => match.test(contact.firstName) || match.test(contact.lastName))
    } else {
      showingContacts = contacts
    }
    if (showingContacts.length) {
      showingContacts.sort(sortBy('lastName'))
    }
    return (
      <div className='list-contacts'>
        <div className='list-contacts-top'>
        <Link
            to='/login'
            className='logout-contact'
          onClick={(e)=> { if(window.confirm('Really logout?')) {
                          this.logout() }
                          else { e.preventDefault() }
          }} >Logout</Link>
          <input
            className='search-contacts'
            type='text'
            placeholder='Search contacts'
            value={query}
            onChange={(event) => this.updateQuery(event.target.value)}
          />
          <Link
            to='/create'
            className='add-contact'
          >Add Contact</Link>
        </div>
         { showingContacts.length !== contacts.length && (
          <div className='showing-contacts'>
            <span>Now showing {showingContacts.length} of {contacts.length} total</span>
            <button onClick={this.clearQuery}>Show all</button>
          </div>
        )}
        
        { showingContacts.length ?  (
        <ol className='contact-list'>
          { showingContacts.map((contact) => (
            <li key={contact.id} className='contact-list-item'>
             
                <div onClick={() =>this.showDetail(contact)} className='contact-avatar' style={{
                  backgroundImage: `url(${process.env.REACT_APP_CONTACTS_API_URL}${contact.avatarURL})`
                }}/>
                <div className='contact-details' onClick={() =>this.showDetail(contact)}>
                  <p>{contact.firstName} {contact.lastName}</p>
                </div>
              
              <button onClick={() => { if(window.confirm('Delete contact?')) {onDeleteContact(contact)}}} className='contact-remove'>
                Remove
              </button>
            </li>
          )) }
        </ol> ) : ( <label>No contacts found. Please add some. </label>) }
      </div>
    )
  }
}

export default ListContacts
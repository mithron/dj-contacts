Django==1.9
django-cors-headers==2.1.0
django-leaflet==0.22.0
djangorestframework==3.6.4
djangorestframework-gis==0.11.2
psycopg2==2.7.3.2
requests>=2.16.2

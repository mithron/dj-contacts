# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-11-08 09:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0003_auto_20171107_1353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactdetail',
            name='contact',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='details', to='contacts.Contact'),
        ),
    ]

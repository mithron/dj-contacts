from rest_framework import permissions


class IsContactOwner(permissions.BasePermission):
    """
    only allow owners of an object.
    """

    def has_object_permission(self, request, view, obj):
        
        # permissions are only allowed to the owner of the contact.
        return obj.user == request.user
        
class IsDetailOwner(permissions.BasePermission):
    """
    only allow owners of contact.
    """

    def has_object_permission(self, request, view, obj):
        
        # permissions are only allowed to the owner of the contact.
        return obj.contact.user == request.user
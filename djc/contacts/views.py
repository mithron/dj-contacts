from rest_framework import generics
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny

from django.contrib.auth.models import User

from .models import Contact, ContactDetail
from .apis import ContactSerializer, DetailSerializer, UserSerializer
from .permissions import IsContactOwner, IsDetailOwner

import logging
import os

from django.views.generic import View
from django.http import HttpResponse
from django.conf import settings
from django.contrib.auth.models import User

# Create your views here.
logger = logging.getLogger(__name__)



class ContactList(generics.ListCreateAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    authentication_classes = (SessionAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsContactOwner,)
    
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
    
    def perform_update(self, serializer):
        serializer.save(user=self.request.user)
    

class ContactDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsContactOwner,)
    
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
    
    def perform_update(self, serializer):
        serializer.save(user=self.request.user)
    
class RemoveDetails(generics.DestroyAPIView) :
    queryset = ContactDetail.objects.all()
    serializer_class = DetailSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsDetailOwner)


class CreateUser(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer


class FrontendAppView(View):
    """
    Serves the compiled frontend entry point 
    """

    def get(self, request):
        try:
            with open(os.path.join(settings.REACT_APP_DIR, 'build', 'index.html')) as f:
                return HttpResponse(f.read())
        except Exception as exc:
            logging.exception('Error occured: %s' % repr(exc))
            return HttpResponse(
                """
                This URL is only used when you have built the production
                version of the app. Visit http://localhost:3000/ instead, or
                run `yarn run build` to test the production version.
                """,
                status=501,
            )
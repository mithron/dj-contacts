from __future__ import unicode_literals

from django.contrib.gis.db import models

from django.contrib.auth.models import User


# Create your models here.
CHOICES = [
        ('Phone', "Phone number"),
 #       ('email', "Email address"),
    ]


class Contact(models.Model):
    user = models.ForeignKey(User, related_name='contacts', on_delete=models.CASCADE)
    firstName = models.TextField(blank=True, null=True)
    lastName = models.TextField(blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    location = models.PointField(blank=True, null=True, srid=3857)
    organization = models.TextField(blank=True, null=True)
    
    objects = models.GeoManager()
    
    def __unicode__(self):
        return (self.firstName if self.firstName else 'Noname') + ' ' + (self.lastName if self.lastName else 'Nolastname')

    
class ContactDetail(models.Model):
    detailName = models.CharField(choices = CHOICES, max_length=40)
    detailValue = models.TextField()
    contact = models.ForeignKey(Contact, related_name='details', on_delete=models.CASCADE)
    
    def __unicode__(self):
        return self.detailName + ': ' + self.detailValue 
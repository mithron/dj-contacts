
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.models import User

from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from .models import Contact, ContactDetail
from .apis import ContactSerializer, DetailSerializer

import logging
# Create your tests here.

class CreateContactTest(APITestCase):
    def setUp(self):
        self.superuser = User.objects.create_superuser('django_test_admin', 'me@example.com', 'django_test_admin_password')
        self.client.force_authenticate(user=self.superuser)
        self.data = {'user': self.superuser, 'firstName': 'John', 'lastName': 'McClane'}

    def test_can_create_contact(self):
        response = self.client.post(reverse_lazy('contacts:contact-list'), self.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ReadContactTest(APITestCase):
    def setUp(self):
        self.superuser = User.objects.create_superuser('django_test_admin', 'me@example.com', 'django_test_admin_password')
        self.client.force_authenticate(user=self.superuser)
        self.data = {'user': self.superuser, 'firstName': 'John', 'lastName': 'McClane'}
        self.contact = Contact.objects.create(**self.data)
        
    def test_can_read_contact_list(self):
        response = self.client.get(reverse_lazy('contacts:contact-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_read_contact_detail(self):
        response = self.client.get(reverse_lazy('contacts:contact-detail', args=[self.contact.id]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UpdateContactTest(APITestCase):
    def setUp(self):
        self.superuser = User.objects.create_superuser('django_test_admin', 'me@example.com', 'django_test_admin_password')
        self.client.force_authenticate(user=self.superuser)
        self.data = {'user': self.superuser, 'firstName': 'John', 'lastName': 'McClane' }
        self.contact = Contact.objects.create(**self.data)
        self.data = ContactSerializer(self.contact).data
        self.data.update({'firstName': 'Changed', 'location': {'type': 'Point', 'coordinates': [62.601, 29.763]}})

    def test_can_update_contact(self):
        response = self.client.put(reverse_lazy('contacts:contact-detail', args=[self.contact.id]), self.data, format='json')
        logging.debug(response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        


class DeleteContactTest(APITestCase):
    def setUp(self):
        self.superuser = User.objects.create_superuser('django_test_admin', 'me@example.com', 'django_test_admin_password')
        self.client.force_authenticate(user=self.superuser)
        self.data = {'user': self.superuser, 'firstName': 'John', 'lastName': 'McClane'}
        self.contact = Contact.objects.create(**self.data)

    def test_can_delete_contact(self):
        response = self.client.delete(reverse_lazy('contacts:contact-detail', args=[self.contact.id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
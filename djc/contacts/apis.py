
import logging
from rest_framework import serializers
from models import Contact, ContactDetail
from django.contrib.auth.models import User


# apis created 

logger = logging.getLogger()

class DetailSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ContactDetail
        fields = [ 'id', 'detailName', 'detailValue']
        
class UserSerializer(serializers.ModelSerializer):
    
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('username', 'password', 'auth_token')
        write_only_fields = ('password',)
        read_only_fields = ('auth_token',)


    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        user.refresh_from_db()
        return user

        

class ContactSerializer(serializers.ModelSerializer):
    
    details = DetailSerializer(many=True, required=False)
    
    class Meta:
        model = Contact
        fields = [ 'id','firstName', 'lastName', 'details', 'address', 'organization', 'location' ] 
        
    def create(self, validated_data):
        try:
            details_data = validated_data.pop('details')
        except KeyError:
            details_data = []
        contact = Contact.objects.create(**validated_data)
        for detail in details_data:
            ContactDetail.objects.create(contact=contact, **detail)
        return contact
    
    def update(self, instance, validated_data, *args, **kwargs):
        try:
            details_data = validated_data.pop('details')
        except KeyError:
            details_data = []
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        curDetailsIds = [item[0] for item in ContactDetail.objects.filter(contact=instance).values_list('pk')] 
        for detail in details_data:
            if detail.get('id'):
                curDetailsIds.remove(detail['id'])
                detailInstance, created = ContactDetail.objects.update_or_create(contact=instance, pk=detail['id'], defaults=detail)
            else:
                detailInstance = ContactDetail.objects.create(contact=instance, **detail)
        ContactDetail.objects.filter(pk__in=curDetailsIds).delete()
        instance.refresh_from_db()
        logger.debug("updated instance %s" % repr(instance))
        return instance
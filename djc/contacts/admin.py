from django.contrib.gis import admin
from leaflet.admin import LeafletGeoAdmin 

# Register your models here.
from .models import Contact, ContactDetail

class DetailInline(admin.StackedInline):
    model = ContactDetail

class ContactAdmin(LeafletGeoAdmin):
    model = Contact
    inlines = [ DetailInline ]

admin.site.register(Contact, ContactAdmin)

admin.site.register(ContactDetail)